package com.jboothwebdev.budgetshopping

import android.app.Application
import com.jboothwebdev.budgetshopping.database.AppDatabase

class TripApplication: Application(){
    val database: AppDatabase by lazy {AppDatabase.getDatabase(this)}
}
