package com.jboothwebdev.budgetshopping.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jboothwebdev.budgetshopping.ShoppingListApplication
import com.jboothwebdev.budgetshopping.TripApplication
import com.jboothwebdev.budgetshopping.adapters.TripAdapter
import com.jboothwebdev.budgetshopping.databinding.FragmentTripBinding
import com.jboothwebdev.budgetshopping.viewmodels.TripViewModel
import com.jboothwebdev.budgetshopping.viewmodels.TripViewModelFactory
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

class TripFragment: Fragment() {
    private var _binding: FragmentTripBinding? = null

    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    private val viewModel: TripViewModel by activityViewModels{
        TripViewModelFactory(
            (activity?.application as ShoppingListApplication).database.TripDao()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTripBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.tripRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val tripAdapter = TripAdapter(viewModel)
        recyclerView.adapter = tripAdapter
        lifecycle.coroutineScope.launch{
            viewModel.getTrips().collect{
                tripAdapter.submitList(it)
            }

        }
        binding.createTripButton.setOnClickListener{
            val action = TripFragmentDirections.actionTripFragmentToSetTripBudgetFragment()
            view.findNavController().navigate(action)
        }

    }
}
